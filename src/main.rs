use axum::{
    extract::Path,
    Json,
    Router, routing::{get, post},
};
use axum::http::StatusCode;
use serde_derive::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
struct Greeting {
    greet: String,
    name: String
}
#[derive(Deserialize, Serialize)]
struct WhateverText {
    input: String,
    name: String
}

// Task 1: Handler for printing hello world and StatusCode when method.GET is used for endpoint "/hello"
async fn hello() -> String {
    format!("Hello, World! {}", StatusCode::OK.to_string())
}

// Task 2: Handler for printing hello {name} when method.GET is used for endpoint "/greet/{name}"
// A handler using path extractors to get the name variable
async fn greet_user(Path(name): Path<String>) -> Json<Greeting> {
    let greeting_struct = Greeting { greet: "Hello ".to_string(), name: name.to_string() };

    Json(greeting_struct)
}

// Task 3: Handler for printing {whatever text} {name} (as a json encoded struct) when method.POST is used for endpoint "/greetme"
// A handler for "/greetme" endpoint. Reads body to WhateverText struct, parses and returns Greeting struct as json
async fn greet_me(Json(item): Json<WhateverText>) -> Json<Greeting> {
    let greeting_struct = Greeting { greet: item.input.to_string(), name: item.name.to_string() };

    // Returns json encoded struct
    Json(greeting_struct)
}

// Main entry point
#[tokio::main]
async fn main() {
    // Adding a new axum router for the endpoints "/hello", "/greet/{name}" and "/greetme", returns hello world and status 200 ok
    let app = Router::new()
        .route("/hello", get(hello))
        .route("/greet/:name", get(greet_user))
        .route("/greetme", post(greet_me));

    // TCP socket server listening for connections
    let listener = tokio::net::TcpListener::bind("0.0.0.0:8000").await.unwrap();
    // Starting server
    axum::serve(listener, app).await.unwrap();
}