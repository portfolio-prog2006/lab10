# Lab10 - Hello JSON in Rust

More structs in Rust, using Axum and Tokio for Web dev

## Learning objectives for Lab10, as outlined by lecturer

> ### Objectives for Lab10
> The code should be:
> - elegant
> - composable (modular)
> - easy to maintain, update, and modify
> - should avoid using `if`, `match` and `case` expressions

----------------
### Prelim. info

*Crates used for this program:*

- serde = "1.0.115"
- serde_derive = "1.0.115"
- serde_json = "1.0.115"
- axum = "0.7.5"
- #tokio = "1.37.0"
- tokio = { version = "1.37.0", features = ["full"] }


## Task 1 - GET "/hello" returns string: `"Hello, World!"` and `200 OK`

A basic function/routing mechanism that, when the user sends a GET request to the "localhost:8080/hello" endpoint, receives a string `"Hello, world!"` and statusCode `200 OK`

### *Output in postman (or browser) when running the program*
```
Hello, World! 200 OK
```
*And in postman:*

![Result of task 1 using postman](Task1.PNG "Hello World")

## Task 2 - GET "/" returns 404 page not found

Makes sure endpoints that do not exist, returns the statusCode 404 page not found

### *Output in postman (or browser) when running the program*
```
404 page not found
```
*And in postman:*

![Result of task 2 using postman](NotFound.PNG "404 page not found")


## Task 3 - GET "/greet/Mariusz" returns JSON { "greet": "Hello", "name": "Mariusz"} and 200 OK

Parses a parameter from url path to a name type in the greeting struct, and returns this struct as a json. If all went well, the status Code should be 200

### *Output in postman (or browser) when sending get req to endpoint "/greet/Sarah"*
```
{ "greet": "Hello", "name": "Sarah"}
```
*And in postman:*

![Result of task 3 using postman](Task2.PNG "Hello Adam")


## Task 4 - POST "/greetme" taking json input sending json output

function that receives a post request to the /greetme endpoint, parses the json in the body of the request to a struct, extracts the name and inputs this to a new struct. 
This new struct is returned as json to the user

### *Output in postman (or browser) when using POST with the json { "input": "whatever text", "name": "Mariusz"}*
```
{"greet": "whatever text", "name": "Mariusz"}
```
*And in postman:*

![Result of task 3 using postman](Task3.PNG "greetme function")



